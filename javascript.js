//load Shop content: 
async function loadItems(itemType) {
  const itemData = await fetchData(itemType);
  itemData.forEach(createShopItem);
  updateBasketIndicator();
}


//fetch shop content-data from json file:
async function fetchData(itemType) {
  try {
    const response = await fetch('/data.json');
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return data.filter(item => item.type === itemType); //filter for specified type of item (pizza, salad or softdrink)
  } catch (error) {
    console.error('Error fetching pizza data:', error);
    return [];
  }
}


//ceate shop item-elements:
function createShopItem(itemData) {
  const container = document.querySelector('.AllShopContainer');
  const shopItem = document.createElement('div');
  shopItem.classList.add('ShopItem');
  switch (itemData.type) { //what type of shop-window should be implemented?
    case "pizza":
      shopItem.innerHTML = `
        <img class="ShopItemPicture" src="${itemData.image}" alt="Picture of ${itemData.name}">
        <div class="ShopSecondRow">
          <div class="ShopItemInfo">
            <h2>${itemData.name}</h2>
            ${itemData.description}
          </div>
          <div class="ShopItemPrize">
            <h2>${itemData.price}$</h2>
            <button onclick="buyItem('${itemData.name}')" class="BuyButton"><img class="CartIcon" src="images/cart_icon.jpg" ></button>
          </div>
        </div>
        `;
      break;
    case "salad":
      shopItem.innerHTML = `
        <img class="ShopItemPicture" src="${itemData.image}" alt="Picture of ${itemData.name}">
        <div class="ShopSecondRow">
          <div class="ShopItemInfo">
            <h2>${itemData.name}</h2>
            ${itemData.description}
            <select class="DropdownSelection" id="${itemData.name}">
              <option value="italian dressing">Italian dressing</option>
              <option value="french dressing">French dressing</option>
              <option value="no dressing">No dressing</option>
            </select>
          </div>
          <div class="ShopItemPrize">
            <h2>${itemData.price}$</h2>
            <button onclick="buyItem('${itemData.name}')" class="BuyButton"><img class="CartIcon" src="images/cart_icon.jpg" ></button>
          </div>
        </div>
        `;
      break;
    case "soft_drink":
      shopItem.innerHTML = `
        <img class="ShopItemPicture" src="${itemData.image}" alt="Picture of ${itemData.name}">
        <div class="ShopSecondRow">
          <div class="ShopItemInfo">
            <h2>${itemData.name}</h2>
            <select class="DropdownSelection" id="${itemData.name}">
              <option value="50cl">50cl</option>
              <option value="100cl">100cl</option>
            </select>
          </div>
          <div class="ShopItemPrize">
            <h2>${itemData.price}$</h2>
            <button onclick="buyItem('${itemData.name}')" class="BuyButton"><img class="CartIcon" src="images/cart_icon.jpg" ></button>
          </div>
        </div>
        `;
      break;
  }
  container.appendChild(shopItem);
}


//basket manipulating functions
async function buyItem(itemName) { //data preparation for addToBasket-function
  const item = {
    name: itemName,
    count: 1,
    dressingOrSize: ""
  };
  const response = await fetch('/data.json');
  const data = await response.json();
  const foundItem = data.find(searchedItem => searchedItem.name === itemName);
  if (foundItem && foundItem.type !== "pizza") {
    let dressingOrSizeVar = document.getElementById(itemName);
    item.dressingOrSize = dressingOrSizeVar.value;
  }
  addToBasket(item);
}
function addToBasket(item) { //adds Item to localStorage, if already in localStorage increase counter by one
  let basket = JSON.parse(localStorage.getItem('basket')) || [];
  const existingItem = basket.find(basketItem => basketItem.name === item.name && basketItem.dressingOrSize === item.dressingOrSize);
  if (existingItem) {
    existingItem.count++;
  } else {
    basket.push(item);
  }
  localStorage.setItem('basket', JSON.stringify(basket));
  updateBasket();
}
function deleteBasketEntry(itemName, dressingOrSize) { //deletes specific item out of localstorage (=basket)
  let basket = JSON.parse(localStorage.getItem('basket')) || [];
  const itemIndex = basket.findIndex(basketItem => basketItem.name === itemName && basketItem.dressingOrSize === dressingOrSize);
  if (itemIndex !== -1) {
    basket.splice(itemIndex, 1);
    localStorage.setItem('basket', JSON.stringify(basket));
    updateBasket();
  }
}


//basket sidebar open and close functions
const openButton = document.getElementById('openBasket');
const closeButton = document.getElementById('closeBasket');
const basketItemsList = document.getElementById('basketItems');
const PageContent = document.querySelector('.PageContent');
const Sidebar = document.querySelector('.Sidebar');
function openBasket() {
  Sidebar.style.display = 'flex';
  openButton.style.display = 'none';
  PageContent.style.width = '70vw';
  Sidebar.style.width = '30vw';
  updateBasket();
}
function closeBasket() {
  Sidebar.style.display = 'none';
  openButton.style.display = 'flex';
  PageContent.style.width = '100%';
  updateBasketIndicator();
}
openButton.addEventListener('click', openBasket);
closeButton.addEventListener('click', closeBasket);


//update the basket content (visually)
async function updateBasket() {
  const basket = JSON.parse(localStorage.getItem('basket')) || [];
  const basketItemsList = document.getElementById('basketItems');
  const Sidebar = document.querySelector('.Sidebar');
  const totalContainer = document.getElementById('BasketTotal');
  const checkOutButton = document.getElementById('checkOutButton');
  let totalPrice = 0; //sum of all prices of all items in basket
  basketItemsList.innerHTML = '';
  if (basket.length === 0) {
    const emptyBasketMessage = document.createElement('div');
    emptyBasketMessage.classList.add('EmptyBasket');
    emptyBasketMessage.textContent = 'Your basket is empty.';
    basketItemsList.appendChild(emptyBasketMessage);
  } else {
    for (const item of basket) {
      try {
        const response = await fetch('/data.json');
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        const shopData = await response.json();

        const shopItem = shopData.find(shopItem => shopItem.name === item.name);
        if (shopItem) {
          const itemPrice = shopItem.price;
          const itemContainer = document.createElement('div');
          itemContainer.classList.add('BasketItems');
          let itemDressingOrSize = "";
          if (item.dressingOrSize != "") {
            itemDressingOrSize = "(" + item.dressingOrSize + ")";
          } else {
            itemDressingOrSize = item.dressingOrSize;
          }
          itemContainer.innerHTML = `
            <div class="BasketEntry">
              <div class="BasketCounterName">
                <p>${item.count}</p>
                <p>${item.name} ${itemDressingOrSize}</p>
              </div>
              <div class="BasketPrizeDelete">
                <p class="MoneyTextP">${item.count * itemPrice} $</p>
                <button class="DeleteEntry" onclick="deleteBasketEntry('${item.name}', '${item.dressingOrSize}')"><img class="XImage" src="images/x_image.jpg"></button>
              </div>
            </div>
          `;
          basketItemsList.appendChild(itemContainer);
          totalPrice += item.count * itemPrice;
        } else {
          console.error(`Item '${item.name}' not found in shop data.`);
        }
      } catch (error) {
        console.error('Error fetching shop data:', error);
      }
    }
  }
  if (totalPrice == 0) {
    totalContainer.innerHTML = ``;
    checkOutButton.style.display = 'none';
  } else {
    totalContainer.innerHTML = `
    <p class="TotalP">Total:</p>
    <p class="MoneyTextP">${totalPrice} $</p>
    `;
    checkOutButton.style.display = 'block';
  }
  updateBasketIndicator(); //assure correct indicator-count
}


//update basket content indicator on basket item
function updateBasketIndicator() {
  const basket = JSON.parse(localStorage.getItem('basket')) || [];
  const basketIndicator = document.getElementById('basketIndicator');
  const totalItems = basket.reduce((total, item) => total + item.count, 0);
  if (totalItems == 0) { //only show if there is something in the basket
    basketIndicator.style.display = 'none';
  } else {
    basketIndicator.style.display = 'flex';
  }
  basketIndicator.textContent = totalItems;
}

//submit form button press, changes page to show thank you message
function submitForm() {
  const form = document.querySelector(".FormContainer");
  if (form.checkValidity()) {
    document.getElementById("formContainer").innerHTML = `<h1>Thanks for your feedback!</h1>`;
  }
}

//checkout function, deletes all basket content, closes basket, changes page to show thank you message
function checkOut() {
  localStorage.setItem('basket', [JSON.stringify([])]);
  closeBasket();
  const pageTitle = document.querySelector(".PageTitle");
  const pageContent = document.getElementById("pageContent");
  pageTitle.innerHTML = `Thanks for your order!`;
  pageContent.innerHTML = ``;
}